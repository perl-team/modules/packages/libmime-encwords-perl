libmime-encwords-perl (1.015.0-1) unstable; urgency=medium

  * Import upstream version 1.015.0.
  * Drop patches, both applied upstream.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Wed, 14 Feb 2024 21:04:46 +0100

libmime-encwords-perl (1.014.3-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libmime-encwords-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 18:10:26 +0000

libmime-encwords-perl (1.014.3-3) unstable; urgency=medium

  [ gregor herrmann ]
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Micah Anderson from Uploaders. Thanks for your work!
  * Remove Nathan Handler from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libmime-charset-perl.
    + libmime-encwords-perl: Drop versioned constraint on libmime-charset-perl
      in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 19:31:03 +0100

libmime-encwords-perl (1.014.3-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Remove . from @INC when loading modules dynamically [CVE-2016-1238]
  * Bump Debhelper compat level to 9
  * Declare compliance with Debian policy 3.9.8
  * Fix spelling error in manpage of MIME::EncWords

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 25 Jul 2016 19:52:56 +0200

libmime-encwords-perl (1.014.3-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Drop xz compression for {binary,source} package, set by default by
    dpkg since 1.17.{0,6}.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Update years of packaging copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 17 Oct 2014 16:38:02 +0200

libmime-encwords-perl (1.014.2-1) unstable; urgency=low

  * Import Upstream version 1.014.2
  * Bump dependency on MIME::Charset

 -- Florian Schlichting <fsfs@debian.org>  Sun, 08 Sep 2013 23:28:29 +0200

libmime-encwords-perl (1.014-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 1.014
  * Bump years of upstream copyright
  * Bump the dependency on libmime-charset-perl to 1.010
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0)
  * Remove dh_builddeb -- -Zxz from debian/rules, xz compression is now the
    default
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Sun, 18 Aug 2013 21:51:38 +0200

libmime-encwords-perl (1.012.4-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
  * debian/control: Convert Vcs-* fields to Git.
  * debian/copyright: Formatting changes.
  * Use XZ compression for source and binary packages.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 30 Oct 2011 12:08:05 +0100

libmime-encwords-perl (1.012.3-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Update my email address.

  [ Fabrizio Regalli ]
  * New upstream release
  * Set Standards-Version to 3.9.2
  * Update d/compat to 8
  * Update debhelper to (>= 8)
  * Added myself to Uploaders and Copyright
  * Update year in copyright
  * Update /usr/share/common-licenses/GPL-1 link
  * Removed old patch.

 -- Fabrizio Regalli <fabreg@fabreg.it>  Mon, 11 Jul 2011 18:22:32 +0200

libmime-encwords-perl (1.012-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Added /me to Uploaders
  * Cleaned up copyright - the author's name was cited incorrectly
  * Updated control description

  [ Nathan Handler ]
  * New upstream release (devel version 1.011_02)
  * debian/control:
    - Add myself to list of uploaders
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * Update jawnsy's email address
  * Update ryan52's email address

  [ Ansgar Burchardt ]
  * New upstream release (1.012).
  * debian/copyright: Update years of copyright.
  * debian/copyright: Formatting changes for current DEP-5 proposal.
  * Use source format 3.0 (quilt).
  * Fix spelling errors in documentation.
    + new patch: spelling.patch
  * Bump Standards-Version to 3.8.4 (no changes).
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 19 Jun 2010 09:55:32 +0900

libmime-encwords-perl (1.011-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Rene Mayorga ]
  * debian/control: update my email address.

  [ Ryan Niebur ]
  * New upstream release
  * Add myself to Uploaders
  * Debian Policy 3.8.1
  * debhelper 7
  * machine readable copyright format

 -- Ryan Niebur <ryanryan52@gmail.com>  Mon, 18 May 2009 23:41:38 -0700

libmime-encwords-perl (1.010.101-1) unstable; urgency=low

  * New upstream release.
  * Bump (build) dependency on libmime-charset-perl to >= 1.006.2.
  * debian/control: change my email address.

 -- gregor herrmann <gregoa@debian.org>  Mon, 21 Apr 2008 17:56:06 +0200

libmime-encwords-perl (1.010.10-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: remove wrong comment.
  * debian/copyright: fix typo, add Debian Perl Group.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Wed, 16 Apr 2008 16:49:04 +0200

libmime-encwords-perl (1.010-1) unstable; urgency=low

  * New upstream release.
  * Bump (build) dependency on libmime-charset-perl to >= 1.006.1
    (closes: #475211).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 13 Apr 2008 20:35:00 +0200

libmime-encwords-perl (1.009-1) unstable; urgency=low

  * New upstream release.
  * Bump (build) dependency on libmime-charset-perl to >= 1.004.
  * debian/copyright: change order of stanzas.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Tue, 01 Apr 2008 16:39:50 +0200

libmime-encwords-perl (1.008-1) unstable; urgency=low

  * New upstream release.
  * debian/rules: minor update without functional changes.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 29 Mar 2008 18:22:43 +0100

libmime-encwords-perl (1.007-1) unstable; urgency=low

  * New upstream release
  * Change the copyright file to the _new_ format
  * Add myself to Uploaders

 -- Rene Mayorga <rmayorga@debian.org.sv>  Sun, 23 Mar 2008 23:53:48 -0600

libmime-encwords-perl (1.005-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 17 Mar 2008 20:42:18 +0100

libmime-encwords-perl (1.004-1) unstable; urgency=low

  [ Roberto C. Sanchez ]
  * New upstream release.

  [ Damyan Ivanov ]
  * Make (build-)dependency on libmime-charset-perl requiire at least 1.001
  * add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Sun, 16 Mar 2008 15:49:01 +0200

libmime-encwords-perl (1.000-1) unstable; urgency=low

  [ Damyan Ivanov ]
  * [debian/watch] Stop capturing file extension

  [ gregor herrmann ]
  * New upstream release.
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/rules:
    - delete /usr/lib/perl5 only if it exists (closes: #467909)
    - update based on dh-make-perl's templates
    - don't install README any more (text version of the POD documentation)
  * debian/watch: use dist-based URL.
  * debian/copyright:
    - use version-agnostic download URL
    - assume copyright from README and Changes
  * Set Standards-Version to 3.7.3 (no changes).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 08 Mar 2008 13:28:18 +0100

libmime-encwords-perl (0.040-3) unstable; urgency=low

  * Add libmime-charset-perl to Depends: in debian/control (closes: #435019).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 28 Jul 2007 17:29:07 +0200

libmime-encwords-perl (0.040-2) unstable; urgency=low

  * Add libmime-charset-perl to Build-Depends-Indep (closes: #422529).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 06 May 2007 20:22:55 +0200

libmime-encwords-perl (0.040-1) unstable; urgency=low

  * Initial Release.

 -- Micah Anderson <micah@debian.org>  Mon, 19 Mar 2007 18:57:33 -0600
